using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vector3Angle : MonoBehaviour
{
    public Transform target;

    // prints "close" if the z-axis of this transform looks
    // almost towards the target

    // 모든게 같고, 축 하나만 다를 경우, 90도가 출력
    // 모든게 같고, 축 두개만 다를 경우, 2D의 각도가 출력
    // 축 3개 모두 다를 경우, 3D의 각도가 출력
    // 모든게 같고, z 축만 다를 경우, 0도가 출력 (? 왜 ?)

    void Update()
    {
        Vector3 targetDir = target.position - transform.position;
        float angle = Vector3.Angle(targetDir, transform.forward);

        Debug.Log(angle);

        if (angle < 5.0f)
            print("close");
    }
}
