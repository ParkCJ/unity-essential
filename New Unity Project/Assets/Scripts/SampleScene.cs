using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SampleScene : MonoBehaviour
{
    public GameObject kule;
    public GameObject kule2;

    // Start is called before the first frame update
    void Start()
    {
        kule.GetComponent<Animator>().Play("Entry");
        kule2.GetComponent<Animator>().Play("Entry");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
